# 4. Úkol – Nastavení NAT sítě
### Zadání
Proveďte nastavení „malé domácí“ sítě připojené do Internetu přes dva
směrovače a NAT podle uvedeného schématu s simulátoru GNS3. Použijte
uvedený adresní prostor, který rozdělíte na dvě podsítě podle doporučení ve
schématu.
### Nastavování komponent v GNS3
##### Příprava komponent
Nejdříve je potřeba v novém projektu komponenty vytvořit a pospojovat. Komponenty jsou spojovány podobně jako bylo ukázáno na cvičení.
Je potřeba dva VPCS spojit se switchem, ten spojit s routerem R1, ten spojit s routerem R2 a ten napojit do komponenty NAT. Všechny komponenty jsem umisťoval do prostředí GNS3 VM.
U routerů jsem spojení volil tak aby brány 0/0 byly směrem k NATu, stejně tak u switche.
Výsledek by měl vypadat podobně jako tomu je u obrázku v zadání.

##### Příkazy pro nastavení
Po spuštění všech komponentlze otevřít jejich příkazové konzole a nastavit je.
Prvním blokem příkazů je nastavení routeru R1 aby komunikoval s počítači a ty komunikovaly mezi sebou a nastavení dhcp pool a dns serveru.
```sh
interface GigabitEthernet 1/0
ip address 10.0.1.254 255.255.255.0
no shutdown
exit

ip add 10.2.1.1 255.255.255.0
no sh

ip dhcp excluded-address 10.0.1.254
ip dhcp pool 1
network 10.0.1.0 /24
dns-server 8.8.8.8 8.8.4.4
default-router 10.0.1.254
[Ctrl-Z]
```
Po provedení těchto příkazů je potřeba nastavit počítačům jejich adresu. Rozhodl jsem se pro statické nastavení.
PC1:
```sh
ip 10.2.1.2 255.255.255.0 gateway 10.2.1.1
```
PC2:
```sh
ip 10.2.1.3 255.255.255.0 gateway 10.2.1.1
```
V tomto bodě by mělo být možné pingovat mezi sebou router a počítače.
```sh
ping 10.2.1.1
ping 10.2.1.2
ping 10.2.1.3
```
Nyní je potřeba nastavit cesta mezi routery R1 a R2
Pro R1 je potřeba do jeho konzole zadat:
```sh
config term
interface GigabitEthernet 0/0
ip address 192.168.1.1 255.255.255.252
no shutdown
[Ctrl-Z]
```
Pro R2 pak:
```sh
config term
interface GigabitEthernet 1/0
ip address 192.168.1.2 255.255.255.252
no shutdown
exit

config term
ip route 10.2.1.0 255.255.255.0 192.168.1.1
```
Tim poslednim příkazem je umožněno routeru R2 pingovat počítače.
V tuto chvíli by mělo být možné pingovat ze všech komponent všechny komponenty.
potřebné adresy:
```sh
10.2.1.1
10.2.1.2
10.2.1.3
192.168.1.1
192.168.1.2
```
Nastavení routerů R1 i R2 je dobré si uložit pomocí:
```sh
copy running-config startup-config
```
Nyní je potřeba připojit se k NAT pomocí R2:
```sh
config term
interface GigabitEthernet 0/0
ip address dhcp
no shutdown
exit

interface GigabitEthernet 1/0
ip nat inside
exit

interface GigabitEthernet 0/0
ip nat outside
exit

access-list 100 permit ip 192.168.1.0 0.0.0.3 any
access-list 100 permit ip 10.0.0.0 0.0.255.255 any
access-list 100 permit ip 10.2.0.0 0.0.255.255 any
ip nat inside source list 100 interface GigabitEthernet 0/0 overload
```
Pomocí těchto příkazů je router R2 nastaven tak aby směrem 0/0 předpokládal směr k NAT a směr 1/0 je směrem od NAT. Zároveň je nastaveno aby povolil přístup počítačům k NAT.
Aby bylo možné neznámů adresy automaticky z R1 přesměrovávat na R2 a následně na NAT je nutné nastavit v R1 ip route:
```sh
ip route 0.0.0.0 0.0.0.0 192.168.1.2
```
Nyní by mělo být možné pingovat ze všech počítačů a routerů adresu 8.8.8.8.
Z routerů by mělo být možné pingovat i přímo pomocí DNS, např. "google.com", nebo "seznam.cz".
Aby to bylo možné i u VPCS, je potřeba u nich změnit adresu DNS pomocí:
```sh
ip dns 8.8.8.8
```
V tento okamžik by mělo být možné pingovat z jednotlivých počítačů "google.com" nebo třeba "seznam.cz".

### Závěr
S GNS3 jsem měl poměrně problémy a stejně tak s psi-node. Proto jsem byl před vypracováním nucen reinstalovat GNS3 VM a využít základních komponent VPCS. 
Práce byla nakonec o něco náročnější než jsem ze začátku předpokládal, ale zato si myslím, že rozumím dané problematice o něco lépe.
